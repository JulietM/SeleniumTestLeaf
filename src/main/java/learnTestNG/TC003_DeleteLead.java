package learnTestNG;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC003_DeleteLead extends ProjectMethod{
	@BeforeClass(groups="smoke")
	public void setData()
	{
		testCaseName="DeleteLead";
		testDesc ="Create a Delete Lead";
		testAuthor="Juliet";
		testCategory="Regression";
		fileName="Delete Lead";
	}

	@Test(dataProvider="fetchData",groups="smoke")
	public void DeleteLead(String pNo)
	{
		//login();
		WebElement eleLead = locateElement("lText","Leads");
		click(eleLead);
		WebElement eleFindLead = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLead);
		WebElement elePhone = locateElement("xpath","//span[text()='Phone']");
		click(elePhone);
		/*WebElement elePhoneArea = locateElement("xpath","//input[@name='phoneAreaCode']");
		type(elePhoneArea,"239");*/
		WebElement elePhoneNumber = locateElement("xpath","//input[@name='phoneNumber']");
		type(elePhoneNumber,pNo);
		WebElement eleFLButton = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFLButton);
		WebElement eleCapLead = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleCapLead);
		WebElement eleDelete = locateElement("xpath","//a[text()='Delete']");
		click(eleDelete);
		
		WebElement eleFindLead1 = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLead1);
		WebElement eleLeadID = locateElement("xpath","//label[text()='Lead ID:']/following-sibling::div/input");
		type(eleLeadID,"10509");
		
		WebElement eleFindLead2 = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLead2);
		closeBrowser();
		
		
	}
}
