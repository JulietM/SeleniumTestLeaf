package learnTestNG;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
@Test
public class TC005_DuplicateLead extends SeMethods{
	
	public void DuplicateLead()
	{
		startApp("Chrome","http://leaftaps.com/opentaps/");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmMod = locateElement("lText","CRM/SFA");
		click(eleCrmMod);
		WebElement eleLead = locateElement("lText","Leads");
		click(eleLead);
		WebElement eleFindLead = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLead);
		WebElement eleEmail = locateElement("xpath","//span[text()='Email']");
		click(eleEmail);
		WebElement eleEmailId = locateElement("name","emailAddress");
		type(eleEmailId,"lakshmi@gmail.com");
		WebElement eleFindLeadBut = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLeadBut);
		WebElement eleFRecName = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a");
		String text = eleFRecName.getText();
		System.out.println(text);
		WebElement eleFRecId = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleFRecId);
		WebElement eleDupLeadbut = locateElement("xpath","//a[text()='Duplicate Lead']");
		click(eleDupLeadbut);
		WebElement eleDupLeadFName = locateElement("xpath","//input[@id='createLeadForm_firstName']");
		String Text2=eleDupLeadFName.getAttribute(text);
		
		System.out.println("Duplicate Lead  :"+Text2);
		closeBrowser();
	}

}
