package learnTestNG;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class TC001_CreateLead extends ProjectMethod {

	@BeforeClass(groups="smoke")
	public void setData()
	{
		testCaseName="HomeWork";
		testDesc ="Create a Test Lead";
		testAuthor="Juliet";
		testCategory="Functional";
		fileName="Create Lead";
	}
	//Test(/*invocationCount=2,invocationTimeOut=300000*/
	@Test(groups="smoke", dataProvider="fetchData")

	public void createLead(String cName,String fName, String lName, String pNo,String eMail) throws InterruptedException 
	{
		//login();
		WebElement eleLead = locateElement("lText","Leads");
		click(eleLead);
		WebElement eleCreate = locateElement("lText","Create Lead");
		click(eleCreate); 
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		type(eleCompName, cName);
		WebElement eleFName = locateElement("id","createLeadForm_firstName");
		type(eleFName, fName);


		WebElement eleLName = locateElement("id","createLeadForm_lastName");
		type(eleLName, lName);
		/*WebElement eleDD1 = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDD1,"Cold Call");
		WebElement eleDD2 = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleDD2,"Automobile");*/

		WebElement elePh = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elePh, pNo);		
		WebElement eleEmail = locateElement("id","createLeadForm_primaryEmail");
		type(eleEmail, eMail);
		WebElement eleCrButton = locateElement("xpath","//input[@name='submitButton']");
		click(eleCrButton);

		//switchToWindow(1);
		/*	System.out.println(driver.getTitle());
		Thread.sleep(3000);
		WebElement eleVal = locateElement("xpath","//span[@id='viewLead_firstName_sp']");
		String a=eleVal.getText();
		System.out.println("First Value"+a);


		//System.out.println("Second Value"+b);
		boolean bl=a.equalsIgnoreCase("TATA");
		System.out.println(bl);*/

	}
	/*@DataProvider(name="fetchData")
	public Object[][] getData(String fileName) throws IOException
	{
		/*Object[][] data=new Object[2][5]; 
		//First Set Data
		data[0][0]="TestLeaf";
		data[0][1]="Melvin";
		data[0][2]="M";
		data[0][3]="7545884544";
		data[0][4]="mels@gmail.com";

		//Second Set Data
		data[1][0]="TestLeaf";
		data[1][1]="Jovin";
		data[1][2]="M";
		data[1][3]="7554458855";
		data[1][4]="jovi@gmail.com";
		return data;
		
		Object[][] excelData=learnExcelPOI.getExcelData(fileName);
		return excelData;
	}*/

}
