package learnTestNG;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC002_EditLead extends ProjectMethod {
	@BeforeClass(groups="smoke")
	public void setData()
	{
		testCaseName="EditLead";
		testDesc ="Create a Edit Lead";
		testAuthor="Juliet";
		testCategory="Sanity";
		fileName="Edit Lead";
	}
	@Test(dataProvider="fetchData", groups="smoke")
	public void EditLead(String lId,String cName) throws InterruptedException {
		//login();
		WebElement eleLead = locateElement("lText","Leads");
		click(eleLead);
		WebElement eleFindLead = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLead);
		/* Not able to filter using this element
		WebElement eleFirstName = locateElement("xpath","//div[@id='x-form-el-ext-gen255']/input");
		type(eleFirstName,"Tata"); */
		
		
		WebElement eleFindResult = locateElement("xpath","//div[@id='x-form-el-ext-gen246']/input");
		type(eleFindResult,lId);
		WebElement eleFindLeadBut = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLeadBut);
		Thread.sleep(1000);
		WebElement eleFLButton = locateElement("xpath","//a[text()='"+lId+"']");
		click(eleFLButton);
		//System.out.println(driver.getTitle());
		WebElement eleEdit = locateElement("xpath","//a[text()='Edit']");
		click(eleEdit);
		//how to clear
		WebElement eleCompName = locateElement("xpath","//input[@id='updateLeadForm_companyName']");
		eleCompName.clear();
		type(eleCompName,cName);
		WebElement eleUpdate = locateElement("xpath","//input[@class='smallSubmit']");
		click(eleUpdate);
		closeBrowser();
	}

}
