package learnTestNG;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class learnExcelPOI {

	public static Object[][] getExcelData(String fileName) throws IOException {

		//To Locate the Work Book
		XSSFWorkbook wBook=new XSSFWorkbook("./data/"+fileName+".xlsx");

		//To Locate the Work Sheet

	//	XSSFSheet sheet = wBook.getSheet(fileName);
		
		XSSFSheet sheetAt = wBook.getSheetAt(0);

		//To get the No of rows

		int lastRowNum = sheetAt.getLastRowNum();

		//To get the No of Columns

		short lastCellNum = sheetAt.getRow(0).getLastCellNum();
		
		Object[][] data=new Object[lastRowNum][lastCellNum];
		//to iterate the row no's
		for(int i=1;i<=lastRowNum;i++)
		{
			//To navigate the First Row data
			XSSFRow row = sheetAt.getRow(i);
			//To iterate each column
			for(int j=0; j<lastCellNum;j++)
			{
				//To navigate the column data
				XSSFCell cell = row.getCell(j);
				//To fetch the column value
				String stringCellValue = cell.getStringCellValue();
				System.out.println(stringCellValue);
				data[i-1][j]=stringCellValue;
			}System.out.println("---------------");

		}
		wBook.close();
		return data;
	}

}
