package learnTestNG;

import java.io.IOException;


import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

//import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {
	static ExtentReports extent;
	static ExtentTest test;
	public static String testCaseName, testDesc, testAuthor, testCategory, fileName;
	@BeforeSuite(groups="commn")
	public void reportStatus()
	{
		ExtentHtmlReporter html=new ExtentHtmlReporter("./Reports/Result.html");
		//To Maintain History set True for not False
		html.setAppendExisting(true);
		extent=new ExtentReports();
		extent.attachReporter(html);
	}
	@BeforeMethod(groups="commn")
	public void startTestCaseClass()
	{
		test=extent.createTest(testCaseName,testDesc);
		//To Create Author and Category
		test.assignAuthor(testAuthor);
		test.assignCategory(testCategory);
	}

	public void reportSteps(String desc,String status)
	{

		if(status.equalsIgnoreCase("Pass"))
		{

			test.pass(desc);
		}if(status.equalsIgnoreCase("Fail"))
		{
			test.fail(desc);
		}
		//test.pass("The Demo Sales....entered Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());


	}
	@AfterSuite(groups="smoke")
	public  void reportClose()
	{
		extent.flush();
	}
}
