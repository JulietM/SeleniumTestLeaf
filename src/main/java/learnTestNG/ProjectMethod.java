package learnTestNG;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


public class ProjectMethod extends SeMethods {
@BeforeMethod(groups="smoke")
@Parameters({"browser","URL","userName","passWord"})
	public void login(String browser,String Url,String uName,String pWord)
	{
		startApp(browser,Url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uName);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, pWord);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmMod = locateElement("lText","CRM/SFA");
		click(eleCrmMod);
	}

@DataProvider(name="fetchData")
public Object[][] getData() throws IOException
{
	Object[][] excelData=learnExcelPOI.getExcelData(fileName);
	return excelData;
}

@AfterMethod(groups="smoke")
	public void close()
	{
		closeAllBrowsers();
		
	}
}
