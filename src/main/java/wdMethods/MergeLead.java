package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
@Test
public class MergeLead extends SeMethods {

	public void MergeLead() throws InterruptedException {
		startApp("Chrome","http://leaftaps.com/opentaps");
		WebElement ele1 = locateElement("id","username");
		type(ele1,"DemoSalesManager");
		WebElement ele2 = locateElement("id","password");
		type(ele2,"crmsfa");
		WebElement ele3 = locateElement("class","decorativeSubmit");
		click(ele3);
		WebElement ele4 = locateElement("ltext","CRM/SFA");
		click(ele4);
		WebElement ele5 = locateElement("ltext","Leads");
		click(ele5);
		WebElement ele6 = locateElement("ltext","Merge Leads");
		click(ele6);
		WebElement ele7 = locateElement("xpath","//table[@name='ComboBox_partyIdFrom']/following-sibling::a/img");
		click(ele7);
		switchToWindow(1);
		WebElement ele8 = locateElement("xpath","//label[text()='Lead ID:']/following-sibling::div/input");
		type(ele8,"10274");
		WebElement ele9 = locateElement("xpath","//button[text()='Find Leads']");
		click(ele9);
		Thread.sleep(3000);
		WebElement ele10 = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(ele10);
		Thread.sleep(3000);
		switchToWindow(0);
		WebElement ele11 = locateElement("xpath","//table[@name='ComboBox_partyIdTo']/following-sibling::a/img");
		click(ele11);
		switchToWindow(1);
		WebElement ele12 = locateElement("xpath","//label[text()='Lead ID:']/following-sibling::div/input");
		type(ele12,"10275");
		WebElement ele13 = locateElement("xpath","//button[text()='Find Leads']");
		click(ele13);
		Thread.sleep(3000);
		WebElement ele14 = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(ele14);
		Thread.sleep(3000);
		switchToWindow(0);
		WebElement ele15 = locateElement("ltext","Merge");
		clickWithNoSnap(ele15);
		acceptAlert();
		WebElement ele16 = locateElement("ltext","Find Leads");
		click(ele16);
		WebElement ele17 = locateElement("xpath","//label[text()='Lead ID:']/following-sibling::div/input");
		type(ele17,"10274");
		WebElement ele18 = locateElement("xpath","//button[text()='Find Leads']");
		click(ele18);
		WebElement ele19 = locateElement("xpath","//div[text()='No records to display']");
		verifyExactText(ele19,"No records to display");
		closeBrowser();
	}
}
