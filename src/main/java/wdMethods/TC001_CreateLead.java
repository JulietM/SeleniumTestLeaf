package wdMethods;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test
public class TC001_CreateLead extends SeMethodsRef {
/*
	 @BeforeClass
		public void setData()
		{
			testCaseName="HomeWork";
			testDesc ="Create a Test Lead";
			testAuthor="Juliet";
			testCategory="Functional";
		}*/
	public void createLeadHW() throws InterruptedException 
	{
		startApp("Chrome","http://leaftaps.com/opentaps/");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmMod = locateElement("lText","CRM/SFA");
		click(eleCrmMod);
		WebElement eleLead = locateElement("lText","Leads");
		click(eleLead);
		WebElement eleCreate = locateElement("lText","Create Lead");
		click(eleCreate); 
		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		type(eleCompName, "TCS");
		WebElement eleFName = locateElement("id","createLeadForm_firstName");
		type(eleFName, "Tata");


		WebElement eleLName = locateElement("id","createLeadForm_lastName");
		type(eleLName, "Consultancy");
		WebElement eleDD1 = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDD1,"Cold Call");
		WebElement eleDD2 = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleDD2,"Automobile");
		WebElement elePh = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elePh, "9548755545");		
		WebElement eleEmail = locateElement("id","createLeadForm_primaryEmail");
		type(eleEmail, "babu@testleaf.com");
		WebElement eleCrButton = locateElement("xpath","//input[@name='submitButton']");
	click(eleCrButton);
	
		//switchToWindow(1);
		System.out.println(driver.getTitle());
		Thread.sleep(3000);
		WebElement eleVal = locateElement("xpath","//span[@id='viewLead_firstName_sp']");
		String a=eleVal.getText();
		System.out.println("First Value"+a);
		
		
		//System.out.println("Second Value"+b);
		boolean bl=a.equalsIgnoreCase("TATA");
		System.out.println(bl);
		
	}

}
