package wdMethods;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

@Test
public class TC002_CreateLead extends SeMethods {

	public void CreateLead() throws InterruptedException {

		startApp("Chrome","http://leaftaps.com/opentaps/");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmMod = locateElement("lText","CRM/SFA");
		click(eleCrmMod);
		WebElement eleLead = locateElement("lText","Leads");
		click(eleLead);
		WebElement eleCreate = locateElement("lText","Create Lead");
		click(eleCreate); 
		WebElement eleDD1 = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDD1,"Cold Call");
		WebElement eleDD2 = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleDD2,"Automobile");
		WebElement elePh = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elePh, "9548755545");		
		WebElement eleEmail = locateElement("id","createLeadForm_primaryEmail");
		type(eleEmail, "babu@testleaf.com");
		//Fill all the Text Fields

		WebElement eleCompName = locateElement("id","createLeadForm_companyName");
		type(eleCompName, "TCS");
		WebElement eleFName = locateElement("id","createLeadForm_firstName");
		type(eleFName, "Tata");
		WebElement eleLName = locateElement("id","createLeadForm_lastName");
		type(eleLName, "Consultancy");

		//NAvigate to Another Window

		WebElement elePAcc = locateElement("xpath","//input[@id='createLeadForm_parentPartyId']/following-sibling::a/img");
		click(elePAcc);
		switchToWindow(1);
		WebElement eleAccId = locateElement("xpath","//label[text()='Account ID:']/following-sibling::div/input");
		type(eleAccId, "Company");
		WebElement eleFindElm = locateElement("xpath","//button[text()='Find Accounts']");
		click(eleFindElm);
		Thread.sleep(5000);
		WebElement eleAccID = locateElement("xpath","//a[text()='Company']");
		clickWithNoSnap(eleAccID);


		//System.out.println(driver.getTitle());
		switchToWindow(0);

		//DTPickers  
		WebElement eleBDate = locateElement("id","createLeadForm_birthDate-button");
		clickWithNoSnap(eleBDate);




		/*Set<String> allWin=driver.getWindowHandles();
		List<String> lofWin=new ArrayList<String>();
		 lofWin.addAll(allWin);
		String sWin = lofWin.get(1);
		driver.switchTo().window(sWin);*/

		/*	
	//Drop Down Calling
	WebElement eleDD1 = locateElement("id","createLeadForm_dataSourceId");
	selectDropDownUsingText(eleDD1,"Cold Call");

	
	WebElement eleDD3 = locateElement("id","createLeadForm_currencyUomId");
	selectDropDownUsingText(eleDD3,"INR - Indian Rupee");
	WebElement eleDD4 = locateElement("id","createLeadForm_industryEnumId");
	selectDropDownUsingText(eleDD4,"Computer Software");
	WebElement eleDD5 = locateElement("id","createLeadForm_ownershipEnumId");
	selectDropDownUsingText(eleDD5,"Public Corporation");
	WebElement eleDD6 = locateElement("id","createLeadForm_generalCountryGeoId");
	selectDropDownUsingText(eleDD6,"India");
	Thread.sleep(2000);
	WebElement eleDD7 = locateElement("id","createLeadForm_generalStateProvinceGeoId");
	selectDropDownUsingText(eleDD7,"TAMILNADU");


	WebElement eleFNameLoc = locateElement("id","createLeadForm_firstNameLocal");
	type(eleFNameLoc, "Test");
	WebElement eleLNameLoc = locateElement("id","createLeadForm_lastNameLocal");
	type(eleLNameLoc, "Leaf");
	WebElement eleTitle = locateElement("id","createLeadForm_personalTitle");
	type(eleTitle, "The");
	WebElement eleGTitle = locateElement("id","createLeadForm_generalProfTitle");
	type(eleGTitle, "Mr.");
	WebElement eleDep = locateElement("id","createLeadForm_departmentName");
	type(eleDep, "Selenium Learning");
	WebElement eleRevn = locateElement("id","createLeadForm_annualRevenue");
	type(eleRevn, "One Crore");
	WebElement eleEmpNo = locateElement("id","createLeadForm_numberEmployees");
	type(eleEmpNo, "25");
	WebElement eleSicCode = locateElement("id","createLeadForm_sicCode");
	type(eleSicCode, "600010");
	WebElement eleTicSym = locateElement("id","createLeadForm_tickerSymbol");
	type(eleTicSym, "TL");
	WebElement eleDesc = locateElement("id","createLeadForm_description");
	type(eleDesc, "Welcome To Selenium Test Leaf Learning");
	WebElement eleImpNot = locateElement("id","createLeadForm_importantNote");
	type(eleImpNot, "Thanks for Coming and Learning Selenium");
	//Contact Information
	WebElement eleACode = locateElement("id","createLeadForm_primaryPhoneAreaCode");
	type(eleACode, "Palavanthangal");
	
	WebElement elePhEx = locateElement("id","createLeadForm_primaryPhoneExtension");
	type(elePhEx, "6756");
	WebElement eleConPer = locateElement("id","createLeadForm_primaryPhoneAskForName");
	type(eleConPer, "Babu M");
	
	WebElement eleUrl = locateElement("id","createLeadForm_primaryWebUrl");
	type(eleUrl, "www.testleaf.com");
	//Primary Address
	WebElement eleToPer = locateElement("id","createLeadForm_generalToName");
	type(eleToPer, "Saravanan");
	WebElement eleAtNam = locateElement("id","createLeadForm_generalAttnName");
	type(eleAtNam, "SSP");
	WebElement eleAdd1 = locateElement("id","createLeadForm_generalAddress1");
	type(eleAdd1, "Plot No 20");
	WebElement eleAdd2 = locateElement("id","createLeadForm_generalAddress2");
	type(eleAdd2, "Palavanthangal");

	WebElement eleCity = locateElement("id","createLeadForm_generalCity");
	type(eleCity, "Chennai");
	WebElement elePostal = locateElement("id","createLeadForm_generalPostalCode");
	type(elePostal, "600100");
	WebElement elePostalCode = locateElement("id","createLeadForm_generalPostalCodeExt");
	type(elePostalCode, "100");

		 */



	}

}

