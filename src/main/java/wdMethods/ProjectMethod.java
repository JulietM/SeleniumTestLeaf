package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class ProjectMethod extends SeMethods {
@BeforeMethod
	public void login()
	{
		startApp("Chrome","http://leaftaps.com/opentaps/");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmMod = locateElement("lText","CRM/SFA");
		click(eleCrmMod);
	}
@AfterMethod
	public void close()
	{
		closeAllBrowsers();
		
	}
}
