package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import week5.day1.LearnReport;
@Test
public class TC001_Login extends SeMethods{
	@BeforeClass
	public void setData()
	{
		testCaseName="TC001_Login";
		testDesc ="Login Successfully";
		testAuthor="Vanitha";
		testCategory="Sanity";
	}
	
	
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
	}
	

}








