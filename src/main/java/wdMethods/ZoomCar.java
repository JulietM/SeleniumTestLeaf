package wdMethods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ZoomCar extends SeMethods {
	@BeforeClass
	public void setData()
	{
		testCaseName="ZoomCar";
		testDesc ="ZoomCar Travel";
		testAuthor="Juliet";
		testCategory="Sanity";
	}

	@Test
	public void zoomCar() throws InterruptedException
	{
		startApp("chrome", "https://www.zoomcar.com/chennai");
		Thread.sleep(500);
		WebElement eleStartJourney=locateElement("xpath","//a[text()='Start your wonderful journey']");
		click(eleStartJourney);
		Thread.sleep(500);
		WebElement elePPoint=locateElement("xpath","(//div[@class='items'])[1]");
		click(elePPoint);

		WebElement eleNext=locateElement("xpath","//button[@class='proceed']");
		click(eleNext);
		WebElement eleDate=locateElement("xpath","(//div[@class='text'])[2]");
		click(eleDate);

		WebElement eleNext1=locateElement("xpath","//button[@class='proceed']");
		click(eleNext1);
		/*String stDat=eleDate.getText();
		System.out.println(stDat);*/

		WebElement eleDone=locateElement("xpath","//button[@class='proceed']");
		click(eleDone);
		Thread.sleep(1500);

		//To get the Complete Car Details and Total no of Car's
		List<WebElement> eleCarList=driver.findElementsByXPath("//div[@class='car-list-layout']/div");
		int no=eleCarList.size();
		System.out.println("Total No is "+eleCarList.size());
		//To get Each Record
		/*	List<String> eleList=new ArrayList<String>();

		for(WebElement eachCar:eleCarList)
		{
			eleList.add(eachCar.getText());
			System.out.println("Each Car Details :"+eachCar.getText());
		}
		Collections.sort(eleList);

		System.out.println("First Car :"+eleList.get(0));*/

		//To find the Car Price 

		List<WebElement> eleCarPrice=driver.findElementsByXPath("//div[@class='price']");

		List<String> eleAmount=new ArrayList<String>();

		for(WebElement eachCar:eleCarPrice)
		{
			eleAmount.add(eachCar.getText().replaceAll("\\D", ""));
			//System.out.println("Each Car Details :"+eachCar.getText().replaceAll("\\D", ""));
		}
		Collections.sort(eleAmount);
		int count=eleAmount.size();
		System.out.println("Minimum Amount is :"+eleAmount.get(0));
		System.out.println("Maximum Amount is :"+eleAmount.get(count-1));
		String maxValue=eleAmount.get(count-1);
		//int i=Integer.parseInt(maxValue);
		//System.out.println("Integer Value is "+i);
		WebElement brandName=locateElement("xpath","//*[contains(text(),'"+maxValue+"')]/../../../div/following-sibling::div/h3");
		System.out.println("Brand Name is :"+brandName.getText());

		//Click on  the High to Low price Button
		/*	WebElement eleHtoLButton=locateElement("xpath","//div[text()=' Price: High to Low ']");
		click(eleHtoLButton);
		WebElement eleFRec=locateElement("xpath","(//div[@class='price']/../../../div/following-sibling::div/h3)[1]");
		String brandName=eleFRec.getText();
		System.out.println("Brand Name :"+brandName);
		WebElement eleBNow=locateElement("xpath","(//button[text()='BOOK NOW'])[1]");
		click(eleBNow);*/

		//closeBrowser();

	}

}
