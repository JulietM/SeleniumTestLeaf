package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
@Test
public class TC004_MergeLead extends SeMethodsRef{
	
	public void MergeLead() throws InterruptedException
	{
		startApp("Chrome","http://leaftaps.com/opentaps/");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmMod = locateElement("lText","CRM/SFA");
		click(eleCrmMod);
		WebElement eleLead = locateElement("lText","Leads");
		click(eleLead);
		WebElement eleMerLead = locateElement("xpath","//a[text()='Merge Leads']");
		click(eleMerLead);
		WebElement eleFmLead = locateElement("xpath","//input[@id='partyIdFrom']/following-sibling::a/img");
		click(eleFmLead);
		switchToWindow(1);
		WebElement eleLeadId = locateElement("xpath","//div[@id='x-form-el-ext-gen25']/input");
		type(eleLeadId,"10163");
		WebElement eleFindLeadBut = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLeadBut);
		WebElement eleFindRec = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(eleFindRec);
		switchToWindow(0);
		WebElement eleToLead = locateElement("xpath","//input[@id='partyIdTo']/following-sibling::a/img");
		click(eleToLead);
		switchToWindow(1);
		WebElement eleLeadId2 = locateElement("xpath","//div[@id='x-form-el-ext-gen25']/input");
		type(eleLeadId2,"10164");
		WebElement eleToFindLeadBut = locateElement("xpath","//button[text()='Find Leads']");
		click(eleToFindLeadBut);
		WebElement eleToFindRec = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(eleToFindRec);
		switchToWindow(0);
		WebElement eleMerBut = locateElement("xpath","//a[text()='Merge']");
		clickWithNoSnap(eleMerBut);
		acceptAlert();
		WebElement eleFindLead = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLead);
		WebElement eleLeadId3 = locateElement("xpath","//label[text()='Lead ID:']/following-sibling::div/input");
		type(eleLeadId3,"10163");
		WebElement eleFLButton = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFLButton);
		Thread.sleep(2000);
		closeBrowser();
		
	}

}
