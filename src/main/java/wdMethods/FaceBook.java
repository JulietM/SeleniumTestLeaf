package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FaceBook extends SeMethods {
	@BeforeClass
	public void setData()
	{
		testCaseName="FaceBook";
		testDesc ="FaceBook Login";
		testAuthor="Juliet";
		testCategory="Sanity";
	}
	
	@Test
	public void faceBook() throws InterruptedException
	{
		startApp("chrome", "https://en-gb.facebook.com/login/");
		WebElement eleEmail = locateElement("xpath", "//div[@id='email_container']/input");
		type(eleEmail, "julietmercyl@gmail.com");
		WebElement elePwd = locateElement("xpath", "//input[@id='pass']");
		type(elePwd, "MelJo1412");
		WebElement eleLogin = locateElement("id", "loginbutton");
		click(eleLogin);
		Thread.sleep(3000);
		String ip="TestLeaf";
		WebElement eleSearch = locateElement("xpath", "(//input[@placeholder='Search'])[1]");
		type(eleSearch, ip);
		//String iSearch=getText(eleSearch); why we are not getting value over here
		System.out.println("Input Search Element"+ip);
		WebElement eleSClick = locateElement("xpath", "//button[@class='_42ft _4jy0 _4w98 _4jy3 _517h _51sy _4w97']/i");
		click(eleSClick);
		WebElement eleVerify = locateElement("xpath", "(//div[@class='_52eh _5bcu'])[1]");
		String text = getText(eleVerify);
		
		if(text.contains(ip) || text.equalsIgnoreCase(ip) || text.contains(ip))
		{
			System.out.println("Verfied and the Search Value is :"+text);
		}
		else
		{
			System.out.println("Its not "+text);
		}
		
		WebElement eleLike = locateElement("xpath", "(//button[@type='submit'])[2]");
		String stLike=getText(eleLike);
		if(stLike.equalsIgnoreCase("Like"))
		{
			click(eleLike);
		}
		else if(stLike.contains("") || stLike.contains(null))
		{
		
			System.out.println("Its already Liked");
		}
		WebElement eleTL = locateElement("xpath", "(//div[text()='TestLeaf'])[1]");
		click(eleTL);
		String title = driver.getTitle();
		if(title.contains(ip) || title.equalsIgnoreCase(ip))
		{
			WebElement eleLikes = locateElement("xpath", "(//div[@class='_4bl9'])[4]");
			String stLikes=getText(eleLikes);
			System.out.println("To Capture the Likes Count :"+stLikes);
		}
		closeBrowser();
	}

}
