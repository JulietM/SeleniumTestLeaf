package wdMethods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class FixMyBugs {



	@Test
	public void FixMyBug() throws InterruptedException {

		// launch the browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.myntra.com/");

		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByXPath("//a[text()='Jackets']").click();


		// Find the count of Jackets
		String leftCount = 
				driver.findElementByXPath("//label[@class='common-customCheckbox vertical-filters-label']/span")
				.getText()
				.replaceAll("//D", "");
		System.out.println(leftCount);


		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[@class='common-customCheckbox vertical-filters-label']/input").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		String rightCount = 
				driver.findElementByXPath("//h1[text()='Mens Jackets']/following-sibling::span")
				.getText()
				.replaceAll("//D", "");
		System.out.println(rightCount);

		// If both count matches, say success
		if(leftCount.contains(rightCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		driver.findElementByXPath("//h4[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<String> onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			//System.out.println(productPrice.getText().replaceAll("\\D", ""));
			onlyPrice.add(productPrice.getText().replaceAll("\\D", ""));

		}	//System.out.println(onlyPrice.get(0)	);




		// Sort them 
		Set<String> st1=new TreeSet<String>();
		st1.addAll(onlyPrice);
		List<String> lt1= new ArrayList<String>();
		lt1.addAll(st1);
		//System.out.println(lt1.get(0));
		Collections.sort(lt1);
		System.out.println(lt1.get(0));
		//String max = onlyPrice.get(0);

		//String max = Collections.sort(onlyPrice);

		// Find the top one
		//	System.out.println(max);

		//driver.close();

		// Print Only Allen Solly Brand Minimum Price
		driver.findElementByXPath("//input[@value='Allen Solly']/following-sibling::div").click();

		// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		List<String> onlyPrices = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			onlyPrices.add(productPrice.getText().replaceAll("//D", ""));
		}
		Collections.sort(onlyPrices);
		// Get the minimum Price 
		String min = onlyPrices.get(0);


		// Find the lowest priced Allen Solly
		System.out.println(min);

	}
}

