package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TC002_EditLead extends SeMethodsRef {
	@Test(dependsOnMethods="TC001_CreateLead.createLeadHW")
	public void EditLead() throws InterruptedException {
		startApp("Chrome","http://leaftaps.com/opentaps/");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmMod = locateElement("lText","CRM/SFA");
		click(eleCrmMod);
		WebElement eleLead = locateElement("lText","Leads");
		click(eleLead);
		WebElement eleFindLead = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLead);
		/* Not able to filter using this element
		WebElement eleFirstName = locateElement("xpath","//div[@id='x-form-el-ext-gen255']/input");
		type(eleFirstName,"Tata"); */
		WebElement eleFindLeadBut = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLeadBut);
		Thread.sleep(2000);
		WebElement eleFindResult = locateElement("xpath","//a[text()='10133']");
		click(eleFindResult);
		System.out.println(driver.getTitle());
		WebElement eleEdit = locateElement("xpath","//a[text()='Edit']");
		click(eleEdit);
		//how to clear
		WebElement eleCompName = locateElement("xpath","//input[@id='updateLeadForm_companyName']");
		type(eleCompName,"TCS");
		WebElement eleUpdate = locateElement("xpath","//input[@class='smallSubmit']");
		click(eleUpdate);
		closeBrowser();
	}

}
