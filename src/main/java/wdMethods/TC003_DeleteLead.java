package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TC003_DeleteLead extends SeMethodsRef{

	@Test(dependsOnMethods="TC001_CreateLead.createLeadHW")
	public void DeleteLead()
	{
		startApp("Chrome","http://leaftaps.com/opentaps/");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmMod = locateElement("lText","CRM/SFA");
		click(eleCrmMod);
		WebElement eleLead = locateElement("lText","Leads");
		click(eleLead);
		WebElement eleFindLead = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLead);
		WebElement elePhone = locateElement("xpath","//span[text()='Phone']");
		click(elePhone);
		/*WebElement elePhoneArea = locateElement("xpath","//input[@name='phoneAreaCode']");
		type(elePhoneArea,"239");*/
		WebElement elePhoneNumber = locateElement("xpath","//input[@name='phoneNumber']");
		type(elePhoneNumber,"9791064364");
		WebElement eleFLButton = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFLButton);
		WebElement eleCapLead = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleCapLead);
		WebElement eleDelete = locateElement("xpath","//a[text()='Delete']");
		click(eleDelete);
		
		WebElement eleFindLead1 = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLead1);
		WebElement eleLeadID = locateElement("xpath","//label[text()='Lead ID:']/following-sibling::div/input");
		type(eleLeadID,"10149");
		
		WebElement eleFindLead2 = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLead2);
		closeBrowser();
		
		
	}
}
