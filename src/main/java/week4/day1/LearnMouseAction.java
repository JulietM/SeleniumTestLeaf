package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnMouseAction {
	static ChromeDriver driver;
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://jqueryui.com/draggable/");
		driver.manage().window().maximize();
		driver.switchTo().frame(0);
		WebElement drg = driver.findElementByXPath("//div[@id='draggable']");
		Actions build=new Actions(driver);
		build.dragAndDropBy(drg, 150, 150).perform();
		//driver.close();

		//To come out frame

		driver.switchTo().defaultContent();
	}

}
