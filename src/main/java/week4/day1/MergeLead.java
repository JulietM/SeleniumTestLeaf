package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MergeLead {
	static ChromeDriver driver;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		driver.findElementByLinkText("CRM/SFA").click();
		//driver.findElementByXPath("//div[@class='crmsfa']/div/a").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("//input[@id='partyIdFrom']/following::a/img").click();
		Set<String> allWin=driver.getWindowHandles();
		List<String> lofWin=new ArrayList<String>();
		 lofWin.addAll(allWin);
		String sWin = lofWin.get(1);
		driver.switchTo().window(sWin);
		String title = driver.getTitle();
		System.out.println(title);
		//driver.findElementById("ext-gen25").sendKeys("10005");
		driver.findElementByXPath("//label[text()='Lead ID:']/following::div/input").sendKeys("10564");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
	//WebDriverWait wait=new WebDriverWait(driver,10);
		//driver.findElementByXPath("//a[text()='10005']").click();
		//driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		/*WebElement table = driver.findElementByXPath("//table[@class='x-grid3-row-table']");
		List<WebElement> tr = table.findElements(By.tagName("tr"));
		WebElement fRow = tr.get(0);
		List<WebElement> fCol = fRow.findElements(By.tagName("td"));
		String text = fCol.get(0).getText();
		System.out.println(text);
		fCol.get(0).click();
		driver.close();
		String fTitle = driver.getTitle();
		System.out.println(fTitle);
		//String fWin = lofWin.get(0);*/

	}

}
