package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWin {
	static ChromeDriver driver;
	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> windowHandles = driver.getWindowHandles();
		System.out.println("Total Windows Count :"+windowHandles.size());

		System.out.println("First Window Title :"+driver.getTitle());

		//To find second Window
		List<String> getWinCount=new ArrayList<String>();
		getWinCount.addAll(windowHandles);

		String secWin = getWinCount.get(1);

		driver.switchTo().window(secWin);
		System.out.println("Second Window Title :"+driver.getTitle());
		String currentUrl = driver.getCurrentUrl();
		System.out.println("Current Window URL is :"+currentUrl);
		driver.quit();
	}

}
