package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class LearnSet {

	public static void main(String[] args) {
Set<String> mbMod=new LinkedHashSet<String>();

mbMod.add("Samsung");
mbMod.add("Onida");
mbMod.add("Samsung");
mbMod.add("LG");
mbMod.add("Sony");
mbMod.add("Panasonic");


//use get, size(index-1);
/*

int i=mbMod.size();
System.out.println(mbMod.get(i-1));*/

List<String> ls=new ArrayList<String>();
ls.addAll(mbMod);
Collections.sort(ls);
int i=ls.size();
for(String eachSt:ls)
{
	System.out.println("iPhone Models are : "+eachSt);
}
//System.out.println("Size of TV Names :"+ls.size());
System.out.println(ls.get(i-1));

	}

}
