package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelTest {
	static ChromeDriver driver;
	public static void main(String[] args) throws Exception {
	
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		//driver.manage().window().maximize();
		SelTest sTes=new SelTest();
		//sTes.findLink();
	
	sTes.pageNavigation();
	sTes.fillPageText();
	}
	
	public void findLink()
	{
		
		driver.get("http://leaftaps.com/opentaps/");
		List<WebElement> findTag=driver.findElementsByTagName("a");
		System.out.println(findTag.size());
		findTag.get(1).click();
	}
	
	public void pageNavigation() throws Exception
	{
	
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
	}
	public void fillPageText() throws Exception 
	{
		driver.findElementById("createLeadForm_companyName").sendKeys("TCS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Melvin");
		driver.findElementById("createLeadForm_lastName").sendKeys("Samuel");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("MELS");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Sam");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr.");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Manager");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Computer Science");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("25 Lakhs");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("20");
		driver.findElementById("createLeadForm_sicCode").sendKeys("191821");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("AIG");
		driver.findElementById("createLeadForm_description").sendKeys("Welcome to Insurance Domain");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Property and Casualty Insurance");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("1");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("600100");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("04466163000");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("63166");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Melvin");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("MelSam@tcs.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.tcs.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Martin");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Markus");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("Siruseri SIPCOT");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Semmeacherry");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("020");
		// Creating object for element
		//WebElement dd1=driver.findElementById("createLeadForm_dataSourceId");
		Select selOpt1=new Select(driver.findElementById("createLeadForm_dataSourceId"));
		selOpt1.selectByVisibleText("Conference");
		WebElement dd2=driver.findElementById("createLeadForm_marketingCampaignId");
		Select selOpt2=new Select(dd2);
		selOpt2.selectByValue("CATRQ_CARNDRIVER");
		Select dd3=new Select(driver.findElementById("createLeadForm_industryEnumId"));
		dd3.selectByVisibleText("Computer Software");
		Select dd4=new Select(driver.findElementById("createLeadForm_ownershipEnumId"));
		dd4.selectByValue("OWN_PROPRIETOR");
		Select dd5=new Select(driver.findElementById("createLeadForm_generalCountryGeoId"));
		dd5.selectByValue("IND");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600020",Keys.TAB);
		Select dd6=new Select(driver.findElementById("createLeadForm_generalStateProvinceGeoId"));
		dd6.selectByValue("IN-TN");
		//driver.findElementByName("submitButton").click();
		List<WebElement> allOpt=selOpt2.getOptions();
		
		/*for(WebElement eachVal:allOpt)
		{
			System.out.println("All the Values in the Drop Down are :"+eachVal.getText());
		}*/
	
		try {
			driver.findElementById("ext-gen596").click();
			driver.findElementByName("parentPartyId").sendKeys("accountlimit100");
			//driver.findElementByLinkText("accountlimit100").getText();
		}catch(NoSuchElementException e)
		{
			throw new Exception(); 
		}
	}

}
