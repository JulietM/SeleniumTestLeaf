package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWebTables {
	static ChromeDriver driver;
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://erail.in/");
		driver.findElementByXPath("//input[@id='txtStationFrom']").clear();
		driver.findElementByXPath("//input[@id='txtStationFrom']").sendKeys("MAS",Keys.TAB);
		driver.findElementByXPath("//input[@id='txtStationTo']").clear();
		driver.findElementByXPath("//input[@id='txtStationTo']").sendKeys("SBC",Keys.TAB);
		boolean bt=driver.findElementById("chkSelectDateOnly").isSelected();
		if(bt)
		{
			driver.findElementById("chkSelectDateOnly").click();
		}
		//Creating Table
		WebElement tb=driver.findElementByXPath("//table[@class='DataTable TrainList']");
		//Finding list of rows
		List<WebElement> lRows=tb.findElements(By.tagName("tr"));
		for(WebElement eachR:lRows)
		{
			//WebElement webElement = eachR.findElements(By.tagName("td")).get(1);
			String text = eachR.findElements(By.tagName("td")).get(1).getText();
			System.out.println("Second Column Value is :"+text);
		}
		//WebElement lr=lRows.get(0);
		//List<WebElement> lCols=lRows.f
		
	}

}
